package Orca::Engine;

use strict;
use warnings;

use Carp ();

my $TRUE    = !! 1;
my $FALSE   = !! 0;

my @requires = qw/
    new namespace
    set cas get gets delete flush_all
/;

for my $require ( @requires ) {
    no strict 'refs';
    *{$require} = sub {
        Carp::confess('This method (${require}) is not implemented this class (@{[ ref $self || $self ]}). Please implement `${require}` method.');
    };
}

sub set_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->set( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub cas_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->cas( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub add {
    my ( $self, $key, $value, $expire ) = @_;

    if ( ! defined $self->get($key) ) {
        my $ret = $self->set( $key, $value, $expire );
        return undef    if ( ! defined $ret );
        return $FALSE   if ( ! $ret );
        return $TRUE;
    }

    return $FALSE;
}

sub add_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->add( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub replace {
    my ( $self, $key, $value, $expire ) = @_;

    if ( defined $self->get( $key ) ) {
        my $ret = $self->set( $key, $value, $expire );
        return undef if ( ! defined $ret );
        return $FALSE if ( ! $ret );
        return $TRUE;
    }

    return $FALSE
}

sub replace_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->replace( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub append {
    my ( $self, $key, $value ) = @_;

    if ( ref $value ) {
        warn "Append value is not scalar: ${key} => ${value}";
        return undef;
    }

    my $source = $self->get($key);

    if ( ref $source ) {
        warn "Target value is not scalar: ${key} => ${source}";
        return undef;
    }

    return $self->set( $key, "${source}${value}" );
}

sub append_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->append( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub prepend {
    my ( $self, $key, $value ) = @_;

    if ( ref $value ) {
        warn "Prepend value is not scalar: ${key} => ${value}";
        return undef;
    }

    my $source = $self->get($key);

    if ( ref $source ) {
        warn "Target value is not scalar: ${key} => ${source}";
        return undef;
    }

    return $self->set( $key, "${value}${source}" );
}


sub prepend_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->prepend( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub get_multi {
    my ( $self, @keys ) = @_;
    my %results  = ();

    for my $key ( @keys ) {
        my $ret = $self->get($key);
        $results{$key} = $ret if ( defined $ret );
    }

    return { %results }
}

sub gets_multi {
    my ( $self, @keys ) = @_;
    my %results = ();

    for my $key ( @keys ) {
        $results{$key} = $self->gets($key);
    }

    return { %results };
}

sub incr {
    my ( $self, $key, $incr ) = @_;

    $incr = 1 if ( ! defined $incr );

    if ( $incr != int($incr) || $incr < 0 ) {
        warn "Increment value is not positive integer: ${incr}";
        return undef;
    }

    my $source = $self->get($key);

    if ( $source != int($source) ) {
        warn "Target value is not integer string.";
        return undef;
    }


    $self->set( $key, $source + $incr );
    
    return $self->get($key);
}

sub incr_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        $task = [ $task ] if ( ! ref $task );
        my $ret = $self->incr(@{ $task });
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub decr {
    my ( $self, $key, $decr ) = @_;

    $decr = 1 if ( ! defined $decr );

    if ( $decr != int($decr) || $decr < 0 ) {
        warn "Decrement value is not positive integer: ${decr}";
        return undef;
    }

    my $source  = $self->get($key);

    if ( $source != int($source) ) {
        warn "Target value is not integer string.";
        return undef;
    }

    $self->set( $key, $source - $decr );
    
    return $self->get($key);
}

sub decr_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        $task = [ $task ] if ( ! ref $task );
        my $ret = $self->decr(@{ $task });
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub delete_multi {
    my ( $self, @keys ) = @_;

    my @results = ();
    my %results = ();

    for my $key ( @keys ) {
        my $ret = $self->delete( $key );
        push @results, $ret;
        $results{$key} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

1;

=head1 NAME

Orea::Engine - Base class for Orca engines

=head1 SYNOPSIS

    package MyEngine;
    
    use strict;
    use warnings;
    
    use parent qw/ Orca::Engine /;
    
    sub new         {}
    sub namespace   {}
    sub set         {}
    sub cas         {}
    sub get         {}
    sub gets        {}
    sub delete      {}
    sub flush_all   {}
    
    1;

=head1 DESCRIPTION

This class is base class for L<Orca> engines.

=head1 OREA ENGINE SPECIFICATION

Orca engine must compatible with basic interface of L<Cache::Memcached::Fast>.
So, if you write orca engine class, you must write engine class compatible with L<Cache::Memcached::Fast>.

C<Orca::Engine> provides some methods:

    set_multi
    cas_multi
    add
    add_multi
    replace
    replace_multi
    append
    append_multi
    prepend
    prepend_multi
    get_multi
    gets_multi
    incr
    incr_multi
    decr
    decr_multi
    delete_multi

So, Requirement of you write methods are:

    new
    namespace
    set
    cas
    get
    gets
    delete
    flush_all

L<Orca::Engine::Memory> is an example implementation of Orca engine class.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head2 SEE ALSO

L<Cache::Memcached::Fast>

L<Orca::Engine::Memory>

L<Orca>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
