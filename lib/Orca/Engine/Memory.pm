package Orca::Engine::Memory;

use strict;
use warnings;

use parent qw/ Orca::Engine /;

use Cache::LRU;

sub new {
    my ( $class, @args ) = @_;

    my $self = bless {
        namespace   => q{},
        cache       => {},
        casid       => {},
        args        => [ @args ],
    }, $class;
    
    return $self;
}

sub namespace {
    my $self = shift;
    
    if ( @_ > 0 ) {
        my $ns = shift @_;
           $ns = "${ns}";
        $self->{'namespace'} = $ns;
    }
    
    return $self->{'namespace'};
}

sub __prepare_cache {
    my $self = shift;
    my $ns   = $self->namespace;
    
    if ( ! exists $self->{'cache'}->{$ns} ) {
        $self->{'cache'}->{$ns} = Cache::LRU->new(@{ $self->{'args'} });
    }

    return $self->{'cache'}->{$ns};
}

sub __prepare_casid {
    my $self = shift;
    my $ns   = $self->namespace;
    
    if ( ! exists $self->{'casid'}->{$ns} ) {
        $self->{'casid'}->{$ns} = Cache::LRU->new(@{ $self->{'args'} });
    }
    
    return $self->{'casid'}->{$ns};
}

sub set {
    my ( $self, $key, $value, $expire ) = @_;

    $expire = 0 if ( ! defined $expire );
    my $cache = $self->__prepare_cache;
    my $casid = $self->__prepare_casid;

    my $casnum = $casid->get($key) || 1;
       $casnum++;
    $casid->set($key => $casnum);
    
    $cache->set( $key => {
        value   => $value,
        ctime   => time,
        expire  => $expire,
        casid   => $casnum,
    } );

    return !! 1;
}

sub get {
    my ( $self, $key ) = @_;
    my $cache = $self->__prepare_cache;

    if ( defined( my $store = $cache->get($key) ) ) {
        if ( $store->{'expire'} != 0 ) {
            if ( ( $store->{'ctime'} + $store->{'expire'} ) <= time ) {
                $cache->remove($key);
                return undef;
            }
        }
        
        return $store->{'value'};
    }

    return undef;

}

sub delete {
    my ( $self, $key ) = @_;
    my $cache = $self->__prepare_cache;

    if ( defined( my $result = $cache->remove($key) ) ) {
        return !! 1;
    }

    return !! 0;
}

sub cas {
    my ( $self, $key, $cas, $value, $expire ) = @_;
    my $cache = $self->__prepare_cache;

    if ( defined( my $store = $cache->get($key) ) ) {
        my $casid = $store->{'casid'};
        
        if ( $cas == $casid ) {
            $self->set( $key, $value, $expire );
            return !! 1;
        }
    }

    return !! 0;
}

sub gets {
    my ( $self, $key ) = @_;
    my $cache = $self->__prepare_cache;

    if ( defined( my $store = $cache->get($key) ) ) {
        return [ $store->{'casid'}, $store->{'value'} ];
    }

    return undef;
}

sub flush_all {
    my ( $self ) = @_;

    delete $self->{'cache'};
    delete $self->{'casid'};

    return !! 1;
}

1;

=head1 NAME

Orca::Engine::Memory - Memory engine for Orca

=head1 SYNOPSIS

    my $orca = Orca->new('Memory');
       $orca->set( $key => $value, $expire );
       $orca->get($key);

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Orca>

L<Orca::Engine>

L<Cache::LRU>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
