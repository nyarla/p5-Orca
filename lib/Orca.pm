package Orca;

use strict;
use warnings;

use Carp ();
use Class::Load ();
use Scalar::Util ();

my @methods = qw/
    namespace
    set         set_multi
    cas         cas_multi
    add         add_multi
    replace     replace_multi
    append      append_multi
    prepend     prepend_multi
    get         get_multi
    gets        gets_multi
    incr        incr_multi
    decr        decr_multi
    delete      delete_multi
    flush_all
/;

sub new {
    my $class   = shift;
    my @args     = @_;

    my $self = bless {}, $class;
       $self->engine( @args );

    return $self;
}

sub engine {
    my $self = shift;
    return $self->{'_engine'} if ( @_ == 0 );

    my $engine;

    if ( Scalar::Util::blessed $_[0] ) {
        $engine = $_[0];
    }
    else {
        my $class   = shift @_ or Carp::confess('Engine name or engine instance is not specified.');
        my @args    = @_;
        
        if ( $class !~ s{^[+]}{} ) {
            $class = "Orca::Engine::${class}";
        }
        
        Class::Load::load_class($class)
            if ( ! Class::Load::is_class_loaded($class) );
        
        $engine = $class->new(@args);
    }
    
    for my $method ( @methods ) {
        Carp::confess("Orca engine (@{[ ref $engine ]}) is not implemented `${method}` method.")
            if ( ! $engine->can($method) );
    }
    
    $self->{'_engine'} = $engine;
    
    return $self->{'_engine'};
}

for my $method ( @methods ) {
    no strict 'refs';
    *{$method} = sub {
        my $self    = shift;
        my $engine  = $self->engine;
        my @args    = @_;
        
        if ( $engine->can($method) ) {
            return ( $engine->$method(@args) ) if wantarray;
            return $engine->$method(@args);
        }
        else {
            Carp::confess("Cache engine (@{[ ref $self->engine ]}) is not implemented `${method}` method.");
        }
    }
}

1;

=head1 NAME

Orca - KVS interface adaptor compatible with Cache::Memcached::Fast

=head1 SYNOPSIS

    use Orca;
    
    my $orca = Orca->new( engine => 'Memory' );
    
    $orca->set( $key => $value, $expire );
    $orca->get( $key );

=head1 DESCRIPTION

C<Orca> is interface adaptor of kvs compatible with basic interface of L<Cache::Memcached::Fast>.

So, you can use C<Orca> alternate with L<Cache::Memcached::Fast>.

=head1 METHODS

=head2 new

    my $orca = Orca->new('+MyEngine' => @args);
    my $orca = Orea->new( $engine_instance );

This method is constructor of C<Orca>.

This constructor is passed all arguments to C<engine> method.
So please see C<engine> method document about arguments of constructor.

=head2 engine

    # get
    $orca->engine;
    
    # set
    $orca->engine('Memory');
    $orca->engine('Memcached' => @args);
    $orca->engine('+MyEngine' => @args);
    $orca->engine( Orca::Engine::Memory->new );

This method gets or sets orca backend engine.

Orca engine class must compatible with orca engine specification.
Please see L<Orca::Engine> about orea engine specification.

Argument of set engine:

=over

=item C<$engine_name =E<gt> @args>

C<$engine_name> is class name of orca engine.

This method adds C<Orca::Engine> prefix to C<$engine_name>.
if you do not want to adding prefix, please adding C<+> prefix.

C<@args> is passed constructor of C<$engine_name> class.

=item C<$engine_instnace>

If you passed blessed object to this method,
this method sets blessed object to orca engine.

=back

=head2 namespace

    $cache->namespace('foo');
    
    my $ns = $cache->namespace;

This method sets or gets cache key namespace.

=head2 set

    $cache->set( $key, $value, $expire );

This method stores C<$value> on the cache under C<$key>.

Argument C<$expire> is designated ecpoch time of cache expire time.

Return value:

=over

=item C<true>

Store cache is successed

=item C<false>

Store cache is failed.

=item C<undef>

Some error.

=back

=head2 set_multi

    $cache->set_multi(
        [ $key, $value ],
        [ $key, $value, $expire ]
    );

This method stores multi values.

Return value:

=over

=item In list context

In list context, this method returns C<@results>.

each C<$results[$index]> is the result value corresponding to the argument at position C<$index>.

=item In scalar context.

In scalar context, this method returns hash reference.

where C<$results-E<gt>{$key}> hols the result value.

=back

Plese see L<"set"> about return value means.

=head2 cas

    $cache->cas( $key, $cas, $value );
    $cache->cas( $key, $cas, $value, $expire );

This method stores C<$value> on the cache under C<$key>
if CAS (Consistent Access Storage) value associated with this key is equal to C<$cas>.

You can gets C<$cas> value using L<gets> or L<gets_multi>.

Return value means is equal to L<"set">.

=head2 cas_multi

    $cache->cas_multi(
        [ $key, $cas, $value ],
        [ $key, $cas, $value, $expire ],
        ...
    );

This method stores multi values if CAS value associated with this key is equal to C<$cas>.

Return value:

=over

Return value means are equal to L<"set_multi">.

=head2 add

    $cache->add( $key, $value );
    $cache->add( $key, $value, $expire );

This method stores C<$value> on the cache undef C<$key>
when C<$key> does not exists.

Return value means are equal to L<"set">

=head2 add_multi

    $cache->add_multi(
        [ $key, $value ]
        [ $key, $value, $expire ],
        ...
    );

This method stores multi values when a cache key does not exists.

Return value means are equal to L<"set_multi">

=head2 replace

    $cache->replace( $key, $value )
    $cache->replace( $key, $value, $expire )

This method stores C<$value> on the cache under C<$key>
when C<$key> does exists.

Return value means are equal to L<"set">

=head2 replace_multi

    $cache->replace(
        [ $key, $value ],
        [ $key, $value, $expire ],
    );

This method stores multi values when a cache key does exists.

Return value means are equal to L<"set_multi">

=head2 append

    my $value = 'foo';
    $cache->set( $key, $value );

    $cache->append( $key, 'bar' );
    
    $cache->get($key) # 'foobar'

This method appends C<$value> to currnet value on cache.

C<$key> and C<$value> should be scalars.

Return value means are equal to L<"set">.

=head2 append_multi

    $cache->append_multi(
        [ $key, $value ],
        [ $key, $value ],
        ...
    );

This method appends multi values to currnet values on cache.

Return value means are equal to L<"set_multi">

=head2 prepend

    my $value = 'foo';
    $cache->set( $key, $value );

    $cache->prepend( $key, 'bar' );
    
    $cache->get($key) # 'barfoo'

This method prepends C<$value> to currnet value on cache.

C<$key> and C<$value> should be scalars.

Return value means are equal to L<"set">.

=head2 append_multi

    $cache->prepends_multi(
        [ $key, $value ],
        [ $key, $value ],
        ...
    );

This method prepends multi values to currnet values on cache.

Return value means are equal to L<"set_multi">

=head2 get

    my $value = $cache->get($key);

This method gets a cache value.

Return value is a cache value, or nothing.

=head2 get_multi

    $cache->get_multi(@keys);

This methods gets cache values.

Return value is hash reference. where C<$results-E<gt>{$key}> holds corresponding value.

=head2 gets
    
    $cache->gets($key)

This method gets a CAS id.

Return value is C<[ $cas, $value ]> or nothing.

=head2 gets_multi

    $cache->gets_multi(@keys);

This method gets multi CAS ids.

Return value is C<{ $key =E<gt> [ $cas, $value ] }, ...>.

=head2 incr

    $cache->incr($key);
    $cache->incr($key, $increment);

This method increments a cache value.

Return value is new value of <$key> or false.

=head2 incr_multi

    $cache->incr_multi(
        $key,
        [ $key ],
        [ $key, $increment ],
    );

This method increments multi cache values.

Return value means are equal to L<"set_multi">

=head2 decr

    $cache->decr($key);
    $cache->decr($key, $decr);

This method decrements a cache value.

Return value is new value of <$key> or false.

=head2 decr_multi

    $cache->decr_multi(
        $key,
        [ $key ],
        [ $key, $decrement ],
    );

This method decrements multi cache values.

Return value means are equal to L<"set_multi">

=head2 delete

    $cache->delete($key);

This method deletes a cache value.

Return value means are equal to L<"set">.

=head2 delete_multi

    $cache->delete_multi(@keys);

This method deletes a cache value of keys.

Return value means are equal to L<"set_multi">.

=head2 flush_all

    $cache->flush_all;

This method clears all cache data.

=head1 L<Cache::Memcached::Fast> COMPATIBILITY

Orca has basic compatibility of L<Cache::Memcached::Fast>.

But, Orca has not compatibility some method L<Cache::Memcached::Fast>.

List of not supported:

    enable_compress
    remove
    nowait_push
    server_versions
    disconnect_all
    set_servers
    set_debug
    set_readonly
    set_norehash
    set_compress_threshold
    status

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Cache::Memcached::Fast>

L<Orca::Engine>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
